﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace task2.Controllers
{
    public class HomeController : Controller
    {
        private const string cookieKey = "mydata";

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SubmitForm(string inputValue, DateTime inputDateTime)
        {
            Response.Cookies.Append(cookieKey, inputValue, new CookieOptions
            {
                Expires = inputDateTime
            });

            return RedirectToAction("Index");
        }

        public IActionResult Check()
        {
            string value = Request.Cookies[cookieKey];
            return View(value as object);
        }
    }
}