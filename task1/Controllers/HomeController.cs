﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace task1.Controllers
{
    public class HomeController : Controller
    {
        internal static int _onlineUserCount = 0;

        public IActionResult Index()
        {
            _onlineUserCount++;
            return View(_onlineUserCount);
        }

        public IActionResult DecrementOnlineCount()
        {
            _onlineUserCount--;
            return RedirectToAction("Index");
        }
    }
}